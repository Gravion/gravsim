﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameName1
{
    class Quad
    {
        //topleft of square
        private Vector2D location;
        public Vector2D Location
        {
            get { return location; }
            set { location = value; }
        }

        private double length;
        public double Length
        {
            get { return length; }
            set { length = value; }
        }

        public Quad(double x, double y, double length)
        {
            Vector2D _location = new Vector2D(x, y);
            Location = _location;     
            Length = length;
        }

        public Quad()
        {
            Location = new Vector2D(0, 0);
            Length = 0;
        }

       

        public bool contains(double x, double y)
        {            
            Vector2D otherPosition = new Vector2D(x, y);
            //calculate if within this quadrant                     
            Vector2D SEP = new Vector2D(Location.X + Length, Location.Y + Length);

            if (otherPosition.X > Location.X && otherPosition.X < SEP.X)
            {
                //it's within the minimum and maximum values of x for this quadrant so check Y:
                //(remember that y is increasing as we go down the plane in xna...)
                if (otherPosition.Y > Location.Y && otherPosition.Y < SEP.Y)
                {
                    //it is within the minimum and maximum values of Y for this quadrant
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            
        }

        public Quad QuadNW()
        {
            double length = this.Length / 2;
            double x = this.Location.X;
            double y = this.Location.Y;

            
            Quad returnQuad = new Quad(x, y, length);
            return returnQuad;
        }

        public Quad QuadNE()
        {
            double length = this.Length / 2;
            double x = this.Location.X + length;
            double y = this.Location.Y;

            
            Quad returnQuad = new Quad(x, y, length);
            return returnQuad;
        }

        public Quad QuadSW()
        {
            double length = this.Length / 2;
            double x = this.Location.X;
            double y = this.Location.Y + length;
           
            Quad returnQuad = new Quad(x, y, length);
            return returnQuad;
        }

        public Quad QuadSE()
        {
            double length = this.Length / 2;
            double x = this.Location.X + length;
            double y = this.Location.Y + length;
           
            Quad returnQuad = new Quad(x, y, length);
            return returnQuad;
        }

    }
}
