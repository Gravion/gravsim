﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GameName1
{
    class Atomic
    {
        constants constants = new constants();

        public Atomic(double M, Vector2D L, Vector2D V)
        {
            Mass = M;
            Location = L;            
            Acceleration = new Vector2D(0, 0);           
            Velocity = V;
        }

        private double mass;
        public double Mass
        {
            get { return mass; }
            set { mass = value; }
        }

        private Vector2D location;
        public Vector2D Location
        {
            get { return location; }
            set { location = value; }
        }      

        //this will be the vector caclculated between this object
        //and each object in the scene which will be built up
        private Vector2D acceleration;
        public Vector2D Acceleration
        {
            get { return acceleration; }
            set { acceleration = value; }
        }

      
        private Texture2D tex;
        public Texture2D Tex
        {
            get { return tex; }
            set { tex = value; }
        }

        //use this to ID for debugging processes
        private String name;
        public String Name
        {
            get { return name; }
            set { name = value; }
        }

        //movement for the leap method
        public void Movement(double dt)
        {            
            //initial velocity which then changes into the orbital velocity 
            //i.e we initially "fire" X+ at Nm/s then as gravity takes hold we get orbital paths
            this.Velocity.X += dt * this.Acceleration.X / mass;
            this.Velocity.Y += dt * this.Acceleration.Y / mass;
            this.Location.X += dt * this.Velocity.X;
            this.Location.Y += dt * this.Velocity.Y;
        }

        public void Movement()
        {
            //initial velocity which then changes into the orbital velocity 
            //i.e we initially "fire" X+ at Nm/s then as gravity takes hold we get orbital paths
            this.Velocity.X += this.Acceleration.X;
            this.Velocity.Y += this.Acceleration.Y;
            this.Location.X += this.Velocity.X;
            this.Location.Y += this.Velocity.Y;
        }

        public bool withinQuad(Quad q)
        {
            if (q.contains(Location.X,Location.Y))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //testing
        public Vector2D Velocity;

        public float Scale;

        
    }
}
