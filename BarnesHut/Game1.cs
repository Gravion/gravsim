﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameName1
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager _graphics;
        SpriteBatch _spriteBatch;
        BHTree BHTree;
        Texture2D sun;
        Texture2D earth;
        Texture2D mouse;
        SpriteFont Font1;
        BHTree bhtree;   
        List<Atomic> lstSceneAtomics = new List<Atomic>();
        Atomic A;
        Atomic B;
        Atomic C;
        Atomic D;
        Atomic E;
        Atomic F;
        int testCounter = 0;
        string testString = "";

        MouseState mouseState;
        MouseState lastMouseState;
        constants constants = new constants();


        public Game1()
        {
            _graphics = new GraphicsDeviceManager(this);

            //_graphics.PreferredBackBufferHeight = 1024;
            //_graphics.PreferredBackBufferWidth = 768;
            //_graphics.IsFullScreen = false;

            Content.RootDirectory = "Content";
        }

        // first, have your Game class implement the IGetMouseState interface


        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            //the strength of gravity and its effects are in proportion to distance and mass
            //if you scale the mass down with the distances you should get the same effect.
            //i.e gravity strength of X with Mass 10 units at distance 1 unit to Y
            //will be exactly the same as X with Mass 1 units at distance 0.1 unit to Y

            //question how to determine bounds of object for centre of mass
            //or when does an object become part of a whole.

            lstSceneAtomics.Clear();
            sun = this.Content.Load<Texture2D>("circle");
            earth = this.Content.Load<Texture2D>("earth");
            mouse = this.Content.Load<Texture2D>("mouse");

            //Manual Setup

            //earth 5.97219E+24 
            Vector2D VectorA = new Vector2D(1000, 400);
            Vector2D DimensionA = new Vector2D(8, 8);
            Vector2D DirectionA = new Vector2D(0, 0.13);
            Texture2D TexA = earth;
            A = new Atomic(5.97219E+29, VectorA, DirectionA);
            A.Tex = TexA;
            A.Name = "Earth";
            A.Scale = 0.64f;

            //sun1.989E+30 
            Vector2D VectorB = new Vector2D(730, 500);
            Vector2D DimensionB = new Vector2D(20, 20);
            Vector2D DirectionB = new Vector2D(0.0, 0);
            Texture2D TexB = sun;
            B = new Atomic(5.97219E+30, VectorB, DirectionB);
            B.Tex = TexB;
            B.Name = "Sun";
            B.Scale = 0.64f;


            //random
            Vector2D VectorD = new Vector2D(1310, 400);
            Vector2D DimensionD = new Vector2D(20, 20);
            Vector2D DirectionD = new Vector2D(0, 0.13);
            Texture2D TexD = earth;
            D = new Atomic(5.97219E+29, VectorD, DirectionD);
            D.Tex = TexD;
            D.Name = "Cheesburger";
            D.Scale = 0.64f;

            //random
            Vector2D Vector = new Vector2D(1340, 400);
            Vector2D Dimension = new Vector2D(20, 20);
            Vector2D Direction = new Vector2D(0, -0.13);
            Texture2D Tex = earth;
            E = new Atomic(5.97219E+29, Vector, Direction);
            E.Tex = Tex;
            E.Name = "Big Cheesburger";
            E.Scale = 0.25f;

            lstSceneAtomics.Add(A);
            lstSceneAtomics.Add(B);
            lstSceneAtomics.Add(D);
            lstSceneAtomics.Add(E);
                    

            base.Initialize();

        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            GamePadState gamePadState = GamePad.GetState(PlayerIndex.One);
            KeyboardState keyboardState = Keyboard.GetState();
            lastMouseState = mouseState;
            mouseState = Mouse.GetState();
            int sign = 0;

            //probably going to have to implement a timer so that we have a uniform time stop and then use leapfrog integration
            #region add 100
            if (lastMouseState.LeftButton == ButtonState.Released && mouseState.LeftButton == ButtonState.Pressed)
            {
                for (int i = 0; i < 5; i++)
                {
                    for (int j = 0; j < 5; j++)
                    {
                        Random random = new Random(DateTime.Now.Millisecond);
                        Atomic atomic;
                        Vector2D Vector = new Vector2D(mouseState.X + (i*44), mouseState.Y + (j*44));


                        //test for negative
                        if (random.NextDouble() > 0.5)
                        {
                            sign = -1;
                        }
                        else
                        {
                            sign = 1;
                        }


                        Vector2D InitialVelocity = new Vector2D(0.20, 0.11);
                        Texture2D Tex = earth;
                        atomic = new Atomic(4.97219E+5, Vector, InitialVelocity);
                        atomic.Name = "Random";
                        atomic.Tex = Tex;
                        atomic.Scale = 0.5f;
                        lstSceneAtomics.Add(atomic);
                    }
                }
            }
            #endregion
            #region //keyboard
            if (keyboardState.IsKeyDown(Keys.W))
            {

                B.Location.Y -= 3f;
            }
            if (keyboardState.IsKeyDown(Keys.A))
            {

                B.Location.X -= 3f;
            }
            if (keyboardState.IsKeyDown(Keys.S))
            {

                B.Location.Y += 3f;
            }
            if (keyboardState.IsKeyDown(Keys.D))
            {

                B.Location.X += 3f;
            }
            #endregion
            // Check to see if the user has exited
            if (checkExitKey(keyboardState, gamePadState))
            {
                base.Update(gameTime);
                return;
            }

            for (int i = 0; i < lstSceneAtomics.Count; i++)
            {
                if (lstSceneAtomics[i].Location.X + lstSceneAtomics[i].Velocity.X > 1920 || lstSceneAtomics[i].Location.X + lstSceneAtomics[i].Velocity.X < 0)
                {
                    lstSceneAtomics.RemoveAt(i);
                    i--;
                }

                if (lstSceneAtomics[i].Location.Y + lstSceneAtomics[i].Velocity.Y > 1920 || lstSceneAtomics[i].Location.Y + lstSceneAtomics[i].Velocity.Y < 0)
                {
                    lstSceneAtomics.RemoveAt(i);
                    i--;
                }
            }

           
                CreateTree();
                foreach (Atomic atomic in lstSceneAtomics)
                {
                    bhtree.insert(atomic);
                }

               
                foreach (Atomic atomic in lstSceneAtomics)
                {
                    //atomic.Velocity = constants.VectorAddition(this.Velocity, this.Acceleration);
                    //initial velocity which then changes into the orbital velocity 
                    //i.e we initially "fire" X+ at Nm/s then as gravity takes hold we get orbital paths  

                }  

                foreach (Atomic atomic in lstSceneAtomics)
                {
                    atomic.Acceleration = new Vector2D();                    
                    CalcDistance(atomic);
                }

                //move atomics
                foreach (Atomic atomic in lstSceneAtomics)
                {
                   //atomic.Movement(6000);
                    atomic.Movement();
                }          



        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            _spriteBatch.Begin();
            foreach (Atomic atomic in lstSceneAtomics)
            {
                Vector2 temp = new Vector2((float)atomic.Location.X, (float)atomic.Location.Y);

                float scaleFactor = atomic.Scale;
                Vector2 origin = new Vector2(atomic.Tex.Width / 2, atomic.Tex.Height / 2);
                _spriteBatch.Draw(atomic.Tex,// Texture
                    temp,                    // Position
                    null,                    // Source rectangle
                    Color.White,             // Color
                    0,                       // Rotation
                    origin,                  // Origin
                    scaleFactor,             // Scale
                    SpriteEffects.None,
                    0.94f);                  // Depth

            }

            _spriteBatch.Draw(mouse, new Vector2(mouseState.X, mouseState.Y), Color.White);

            _spriteBatch.End();
            base.Draw(gameTime);
        }

        bool checkExitKey(KeyboardState keyboardState, GamePadState gamePadState)
        {
            // Check to see whether ESC was pressed on the keyboard             
            if (keyboardState.IsKeyDown(Keys.Escape))
            {
                Exit();
                return true;
            }
            return false;
        }

        bool checkToggleKey(KeyboardState keyboardState, GamePadState gamePadState)
        {

            if (keyboardState.IsKeyDown(Keys.Space))
            {

                return true;
            }
            return false;
        }

        public void CreateTree()
        {
            Quad quad = new Quad(0, 0, 1920);
            bhtree = new BHTree(quad);
        }

        void CalcDistance(Atomic atomic)
        {
            //reset acceleration from last frame
            atomic.Acceleration = new Vector2D(0, 0);
            bhtree.findForce(ref atomic);
        }


    }
}
