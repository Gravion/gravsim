﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameName1
{
    class BHTree
    {
        private Atomic atomic; // body or aggregate body stored in this node
        private Quad quad;     // square region that the tree represents
        private BHTree NW;     // tree representing northwest quadrant
        private BHTree NE;     // tree representing northeast quadrant
        private BHTree SW;     // tree representing southwest quadrant
        private BHTree SE;     // tree representing southeast quadrant
        public double aggregateMass;
        public Vector2D aggregateLocation;
        public constants constants = new constants();

        public BHTree(Quad q)
        {
            quad = q;
            NW = null;
            NE = null;
            SW = null;
            SE = null;
            atomic = null;

        }

        //To insert a body b into the tree rooted at node x, use the following recursive procedure:
        //If node x does not contain a body, put the new body b here.
        //If node x is an internal node, update the center-of-mass and total mass of x. Recursively insert the body b in the appropriate quadrant.
        //If node x is an external node, say containing a body named c, then there are two bodies b and c in the same region.
        //Subdivide the region further by creating four children. Then, recursively insert both b and c into the appropriate quadrant(s). 
        //Since b and c may still end up in the same quadrant, there may be several subdivisions during a single insertion. 
        //Finally, update the center-of-mass and total mass of x.
        public void insert(Atomic B)
        {
            if (CheckInternal())
            {
                if (quad.QuadNW().contains(B.Location.X, B.Location.Y))
                {
                    NW.insert(B);

                }
                else if (quad.QuadNE().contains(B.Location.X, B.Location.Y))
                {
                    NE.insert(B);

                }
                else if (quad.QuadSW().contains(B.Location.X, B.Location.Y))
                {
                    SW.insert(B);

                }
                else if (quad.QuadSE().contains(B.Location.X, B.Location.Y))
                {
                    SE.insert(B);

                }

                if (CheckEmptyChildren())
                {
                    //children are empty we need to calculate CoM with aggregate data.
                    NodeMassCentreAggregate();
                }
                else
                {
                    NodeMassCentre();
                }
            }
            else
            {
                if (atomic == null)
                {
                    //external
                    atomic = B;
                }
                else
                {
                    //I am an external node with an item and need to create subdivisions to fit in a new item and my own.
                    //Since we have created new trees I should never be counted as external and thus no new nodes can
                    //be attributed to me
                    NW = new BHTree(quad.QuadNW());

                    NE = new BHTree(quad.QuadNE());

                    SW = new BHTree(quad.QuadSW());

                    SE = new BHTree(quad.QuadSE());

                    insert(B);

                    //acting as a parent node now so we reinsert                    
                    Atomic temp = new Atomic(atomic.Mass, atomic.Location, atomic.Velocity);
                    temp.Name = atomic.Name;
                    temp.Tex = atomic.Tex;
                    insert(temp);
                    atomic = null;


                }
            }
        }

        public bool CheckInternal()
        {
            //if all my children are empty then I am external
            if (NW == null && NE == null && SW == null && SE == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public Atomic CentreOfMass(Atomic a, Atomic b)
        {
            //m = m1 + m2
            //x = (x1m1 + x2m2) / m
            //y = (y1m1 + y2m2) / m

            //lazy null check for nodes
            if (b == null)
            {
                return a;
            }

            if (a == null)
            {
                return b;
            }

            if (a.Location == null && b.Location == null)
            {
                return null;
            }

            double m = a.Mass + b.Mass;
            double x = ((a.Location.X * a.Mass) + (b.Location.X * b.Mass)) / m;
            double y = ((a.Location.Y * a.Mass) + (b.Location.Y * b.Mass)) / m;

            return new Atomic(m, new Vector2D(x, y), new Vector2D(0, 0));
        }

        public void NodeMassCentre()
        {

            Atomic A = new Atomic(0, new Vector2D(0, 0), new Vector2D(0, 0));
            Atomic B = new Atomic(0, new Vector2D(0, 0), new Vector2D(0, 0));
            Atomic C = new Atomic(0, new Vector2D(0, 0), new Vector2D(0, 0));

            A = CentreOfMass(NW.atomic, NE.atomic);
            B = CentreOfMass(SW.atomic, SE.atomic);
            C = CentreOfMass(A, B);

            aggregateLocation = C.Location;
            aggregateMass = C.Mass;
        }

        public void NodeMassCentreAggregate()
        {
            Atomic A;
            Atomic B;
            Atomic C;
            Atomic D;

            //needs to be the aggregate data of the nodes.
            if (NW.aggregateMass != 0.0 && NW.aggregateLocation != null)
            {
                A = new Atomic(NW.aggregateMass, NW.aggregateLocation, new Vector2D(0, 0));
            }
            else
            {
                A = null;
            }

            if (NE.aggregateMass != 0.0 && NE.aggregateLocation != null)
            {
                B = new Atomic(NE.aggregateMass, NE.aggregateLocation, new Vector2D(0, 0));
            }
            else
            {
                B = null;
            }

            if (SE.aggregateMass != 0.0 && SE.aggregateLocation != null)
            {
                C = new Atomic(SE.aggregateMass, SE.aggregateLocation, new Vector2D(0, 0));
            }
            else
            {
                C = null;
            }

            if (SW.aggregateMass != 0.0 && SW.aggregateLocation != null)
            {
                D = new Atomic(SW.aggregateMass, SW.aggregateLocation, new Vector2D(0, 0));
            }
            else
            {
                D = null;
            }


            Atomic E = new Atomic(0, new Vector2D(0, 0), new Vector2D(0, 0));
            Atomic F = new Atomic(0, new Vector2D(0, 0), new Vector2D(0, 0));
            Atomic G = new Atomic(0, new Vector2D(0, 0), new Vector2D(0, 0));

            E = CentreOfMass(A, B);
            F = CentreOfMass(C, D);
            G = CentreOfMass(E, F);

            aggregateLocation = G.Location;
            aggregateMass = G.Mass;

        }

        public void findForce(ref Atomic A)
        {
            //Calculating the force acting on a body. To calculate the net force acting on body b, use the following recursive procedure,
            //starting with the root of the quad-tree:
            //If the current node is an external node (and it is not body b), calculate the force exerted by the current node on b,       
            //Otherwise, calculate the ratio s / d. 
            //Where s is the width of the region represented by the internal node, and d is the distance between the body and the node's center-of-mass.
            //If s / d < θ, treat this internal node as a single body
            //Otherwise, run the procedure recursively on each of the current node's children.

            if (!(CheckInternal()))
            {
                //external node so calc the acceleration vector for A based on this nodes mass
                //possible to be an external node without an atomic as all 4 quadrants are created at once
                if (atomic != null)
                {
                    A = CalcDistance(A, atomic);
                }
                return;
            }
            else
            {
                //internal node check for s/d                

                double d = constants.PointDistance(A.Location, aggregateLocation);

                if ((quad.Length / d) < constants.theta)
                {
                    //s/d < theta so we aggregate this nodes data and calculate the acceleration vector..
                    Atomic nodeCentre = new Atomic(aggregateMass, aggregateLocation, new Vector2D(0, 0));
                    A = CalcDistance(A, nodeCentre);
                }
                else
                {
                    //Repeat for children
                    if (NW != null)
                    {

                        NW.findForce(ref A);
                    }

                    if (NE != null)
                    {

                        NE.findForce(ref A);

                    }

                    if (SW != null)
                    {
                        SW.findForce(ref A);
                    }

                    if (SE != null)
                    {
                        SE.findForce(ref A);
                    }

                }

            }

        }

        public Atomic CalcDistanceLeap(Atomic A, Atomic B)
        {
            //unused atm, can be used for the leapfrog integration solution
            double EPS = 3E4; // softening parameter

            double dx = B.Location.X - A.Location.X;
            double dy = B.Location.Y - A.Location.Y;
            double dist = Math.Sqrt(dx * dx + dy * dy);
            dist *= 1000000;

            if (dist > 0)
            {
                double F = (constants.G * A.Mass * B.Mass) / (dist * dist + EPS * EPS);
                A.Acceleration.X += F * dx / dist;
                A.Acceleration.Y += F * dy / dist;
                return A;
            }
            else
            {
                return A;
            }

        }

        public Atomic CalcDistance(Atomic A, Atomic B)
        {

            double distance = constants.PointDistance(A.Location, B.Location);
            //we check for distance greater than 0 here because newton's formula tends to infinity at zero 
            //so as to reduce the number of objects that fly off due to huge increased acceleration
            if (distance > 1)
            {
                //scale out co-ordinate distance to millions of km e.g 100,100 -> 200,100 = 100million distance on the x axis
                distance *= 100000;
                double distancesquared = distance * distance;

                double m1m2 = A.Mass * B.Mass;

                double force = ((constants.G * m1m2) / distancesquared);

                double acceleration = force / A.Mass;


                //vector between two objects, note that this calculates the vector distance
                //to travel there instantly ...
                Vector2D tempDistanceVector = new Vector2D();
                tempDistanceVector = constants.VectorBetweenPoints(A.Location, B.Location);

                //... so scale the vector for the acceleration we feel in that direction   
                double vectorMagnitude = constants.VectorMagnitude(tempDistanceVector);

                Vector2D vectorScale = new Vector2D(tempDistanceVector.X / vectorMagnitude, tempDistanceVector.Y / vectorMagnitude);

                tempDistanceVector = constants.VectorScale(vectorScale, acceleration);

                //and add it to the current vector we have
                A.Acceleration = constants.VectorAddition(tempDistanceVector, A.Acceleration);

                return A;
            }
            else
            {
                return A;
            }

        }

        public bool CheckEmptyChildren()
        {
            if (NW.atomic == null && SW.atomic == null && NE.atomic == null && SE.atomic == null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}

