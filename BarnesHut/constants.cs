﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GameName1
{
    class constants
    {
        //Universal Gravitational Force Constant  10^-20 gives distances in km /  10^-11 in m
        public readonly static double G = 6.6726 * Math.Pow(10, -20);
        //public readonly static double G = 6.6726 * Math.Pow(10, -11);

        public readonly static double theta = 0.9;

        public float timeSpan;

        public double PointDistance(Vector2D A, Vector2D B)
        {
            double xsquared = (B.X - A.X) * (B.X - A.X);
            double ysquared = (B.Y - A.Y) * (B.Y - A.Y);

            double distance = Math.Sqrt(xsquared + ysquared);

            return distance;
        }

        public Vector2D VectorBetweenPoints(Vector2D A, Vector2D B)
        {
            Vector2D vectorDistance = new Vector2D(B.X - A.X, B.Y - A.Y);
            return vectorDistance;
        }

        public Vector2D VectorScale(Vector2D A, double Scale)
        {            
            //we need to scale distance movement
            //we need to move xm/s in direction of the unit vector? 
            Vector2D vectorScale = new Vector2D(0, 0);
            vectorScale.X = A.X * Scale;
            vectorScale.Y = A.Y * Scale;            
            return vectorScale;
        }

        public Vector2D VectorAddition(Vector2D A, Vector2D B)
        {
            Vector2D vectorAdd = new Vector2D(A.X + B.X, A.Y + B.Y);
            return vectorAdd;
        }

        public double VectorMagnitude(Vector2D A)
        {
            double vectorMag = (A.X * A.X) + (A.Y * A.Y);
            vectorMag = Math.Sqrt(vectorMag);
            return vectorMag;
        }

        public double VectorMagnitudeSquared(Vector2D A)
        {
            double vectorMag = (A.X * A.X) + (A.Y * A.Y);           
            return vectorMag;
        }
    }
}
